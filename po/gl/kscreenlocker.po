# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marce Villarino <mvillarino@kde-espana.es>, 2012.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-06 00:47+0000\n"
"PO-Revision-Date: 2018-04-22 09:16+0100\n"
"Last-Translator: Adrian Chaves <adrian@chaves.io>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, fuzzy, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Marce Villarino"

#, fuzzy, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mvillarino@kde-espana.es"

#: abstractlocker.cpp:39
#, fuzzy, kde-format
#| msgid ""
#| "The screen locker is broken and unlocking is not possible anymore.\n"
#| "In order to unlock it either ConsoleKit or LoginD is needed, none of\n"
#| "which could be found on your system."
msgid ""
"The screen locker is broken and unlocking is not possible anymore.\n"
"In order to unlock it either ConsoleKit or LoginD is needed, neither\n"
"of which could be found on your system."
msgstr ""
"O bloqueador de pantalla está estragado e xa non é posíbel usalo para "
"desbloqueala.\n"
"Para desbloqueala necesita ou ConsoleKit ou LoginD, e non se atopou ningún "
"deles no sistema."

#: abstractlocker.cpp:43
#, fuzzy, kde-format
#| msgid ""
#| "The screen locker is broken and unlocking is not possible anymore.\n"
#| "In order to unlock switch to a virtual terminal (e.g. Ctrl+Alt+F2),\n"
#| "log in as root and execute the command:\n"
#| "\n"
#| "# ck-unlock-session <session-name>\n"
#| "\n"
msgid ""
"The screen locker is broken and unlocking is not possible anymore.\n"
"In order to unlock it, switch to a virtual terminal (e.g. Ctrl+Alt+F%1),\n"
"log in as root and execute the command:\n"
"\n"
"# ck-unlock-session <session-name>\n"
"\n"
msgstr ""
"O bloqueador de pantalla está estragado e xa non é posíbel usalo para "
"desbloqueala.\n"
"Para desbloquear a pantalla, terá que abrir un terminal virtual (por "
"exemplo, premedo Ctrl+Alt+F2),\n"
"identificarse como root e executar a seguinte orde:\n"
"\n"
"# ck-unlock-session <nome-da-sesión>\n"
"\n"

#: abstractlocker.cpp:48
#, fuzzy, kde-format
#| msgid ""
#| "The screen locker is broken and unlocking is not possible anymore.\n"
#| "In order to unlock switch to a virtual terminal (e.g. Ctrl+Alt+F2),\n"
#| "log in and execute the command:\n"
#| "\n"
#| "loginctl unlock-session %1\n"
#| "\n"
#| "Afterwards switch back to the running session (Ctrl+Alt+F%2)."
msgid ""
"The screen locker is broken and unlocking is not possible anymore.\n"
"In order to unlock it, switch to a virtual terminal (e.g. Ctrl+Alt+F%1),\n"
"log in to your account and execute the command:\n"
"\n"
"loginctl unlock-session %2\n"
"\n"
"Then log out of the virtual session by pressing Ctrl+D, and switch\n"
"back to the running session (Ctrl+Alt+F%3).\n"
"Should you have forgotten the instructions, you can get back to this\n"
"screen by pressing CTRL+ALT+F%2\n"
"\n"
msgstr ""
"O bloqueador de pantalla está estragado e xa non é posíbel usalo para "
"desbloqueala.\n"
"Para desbloquear a pantalla, terá que abrir un terminal virtual (por "
"exemplo, premedo Ctrl+Alt+F2),\n"
"identificarse e executar a seguinte orde:\n"
"\n"
"loginctl unlock-session %1\n"
"\n"
"A continación volva á sesión (premendo Ctrl+Alt+F%2)."

#: ksldapp.cpp:164
#, kde-format
msgid "Lock Session"
msgstr "Bloquear a sesión"

#: ksldapp.cpp:391
#, kde-format
msgid "Screen locked"
msgstr "A pantalla está bloqueda"

#: ksldapp.cpp:550
#, kde-format
msgid "Screen unlocked"
msgstr "Desbloqueouse a pantalla"

#: logind.cpp:161
#, kde-format
msgid "Screen Locker"
msgstr "Bloqueador da pantalla"

#: logind.cpp:161
#, kde-format
msgid "Ensuring that the screen gets locked before going to sleep"
msgstr "Asegúrase de que a pantalla se bloquea antes de irse durmir."

#~ msgid "Screen lock enabled"
#~ msgstr "Activouse o bloqueo da pantalla"

#~ msgid "Sets whether the screen will be locked after the specified time."
#~ msgstr ""
#~ "Determina se se bloqueará ou non a pantalla pasado o tempo indicado."

#~ msgid "Screen saver timeout"
#~ msgstr "Tempo de agarda do protector de pantalla"

#~ msgid "Sets the minutes after which the screen is locked."
#~ msgstr ""
#~ "Determina o número de minutos que deben pasar para que se bloquee a "
#~ "pantalla."

#~ msgid "<qt><nobr><b>Automatic Log Out</b></nobr></qt>"
#~ msgstr "<qt><nobr><b>Saír automaticamente</b></nobr></qt>"

#~ msgid ""
#~ "<qt>To prevent being logged out, resume using this session by moving the "
#~ "mouse or pressing a key.</qt>"
#~ msgstr ""
#~ "<qt>Para evitar ser votado do sistema, continúe usando esta sesión "
#~ "movendo o rato ou premendo unha tecla.</qt>"

#~ msgid "Time Remaining:"
#~ msgstr "Tempo restante:"

#~ msgid ""
#~ "<qt><nobr>You will be automatically logged out in 1 second</nobr></qt>"
#~ msgid_plural ""
#~ "<qt><nobr>You will be automatically logged out in %1 seconds</nobr></qt>"
#~ msgstr[0] ""
#~ "<qt><nobr>A súa sesión terminará automaticamente en 1 segundo.</nobr></qt>"
#~ msgstr[1] ""
#~ "<qt><nobr>A súa sesión terminará automaticamente en %1 segundos.</nobr></"
#~ "qt>"

#~ msgid "Enable screen saver"
#~ msgstr "Activar o protector de pantalla"

#~ msgid "Enables the screen saver."
#~ msgstr "Activa o protector de pantalla."

#~ msgid "Suspend screen saver when DPMS kicks in"
#~ msgstr "Suspender o protector de pantalla se DPMS entra en acción"

#~ msgid ""
#~ "Usually the screen saver is suspended when display power saving kicks "
#~ "in,\n"
#~ "       as nothing can be seen on the screen anyway, obviously. However, "
#~ "some screen savers\n"
#~ "       actually perform useful computations, so it is not desirable to "
#~ "suspend them."
#~ msgstr ""
#~ "Polo xeral o protector suspéndese cando o aforro de enerxía da pantalla "
#~ "entra en acción,\n"
#~ "xa que non se poderá ver nada na pantalla. Porén algúns protectores fan\n"
#~ "cálculos de utilidade, polo que non é desexábel suspendelos."

#~ msgid "Enable legacy X screen saver"
#~ msgstr "Activar o protector de pantalla herdado das X"

#~ msgid "Uses an X screensaver with the screen locker."
#~ msgstr "Usa un protector de pantalla das X co trancador."
